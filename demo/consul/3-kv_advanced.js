const consul = require('consul')
const _ = require('lodash')

function Consul(options = {}) {
  if (!(this instanceof Consul)) {
    return new Consul(options)
  }
  this._consul = consul({
    ...options,
    promisify: true
  })
}

Consul.prototype.store = async function (path, data) {
  this._consul.kv.set(path, JSON.stringify(data))
}

Consul.prototype.storeSeedData = async function storeSeedData(seedData) {
  return Promise.all(Object.keys(seedData).map(path => {
    return this.store(path, seedData[path])
  }))
}

Consul.prototype.getPrefixedKeys = async function (prefix) {
  return this._consul.kv.get({key: prefix, recurse: true})
}

Consul.prototype.combine = function (consulResults) {
  return consulResults.reduce((agg, kv) => {
    return _.set(agg, kv.Key.split('/').join('.'), JSON.parse(kv.Value))
  }, {})
}

Consul.prototype.getPrefixedKeysAndCombine = async function (prefix) {
  const prefixedKeys = await this.getPrefixedKeys()
  return this.combine(prefixedKeys)
}

Consul.prototype.deletePrefixedKeys = async function (prefix) {
  return this._consul.kv.del({key: prefix, recurse: true})
}

const PREFIX = 'granny'
const seedData = {
  [`${PREFIX}/grandson/statement`]: 'Grannies are for pwning noobs!',
  [`${PREFIX}/response`]: 'Grannies are for pwning noobs!',
  [`${PREFIX}/pie`]: {
    request: 'Make me a pie!',
    status: 'preheating',
    temp: 425,
    ingredients: ['strawberries', 'sugar']
  },
  [`${PREFIX}/passive-aggressive/response`]: 'I hope you like it, its strawberry!'
}

async function storeAndRetrieve() {
  const instance = new Consul()
  await instance.storeSeedData(seedData)
  const rawRecursiveResults = await instance.getPrefixedKeys(PREFIX)

  console.log('\nResult:', rawRecursiveResults)

  let configFromConsul = instance.combine(rawRecursiveResults)
  console.log('\nBaked Results: ', JSON.stringify(configFromConsul, null, 2))

  await instance.deletePrefixedKeys(PREFIX)
}

storeAndRetrieve()
