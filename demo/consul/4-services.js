const consul = require('consul')({promisify: true})

const options = {service: 'vault'}

async function start() {

  await consul.catalog.service.list().then(result => {
    console.log('services registered: ', result)
  })

  await consul.catalog.service.nodes(options).then(result => {
    console.log('vaults service info: ', result)
  })

  await consul.health.checks(options).then(result => {
    console.log('vault health info: ', result)
  })
}

start()
