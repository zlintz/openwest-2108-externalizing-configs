const fs = require('fs')
const options = {
  apiVersion: 'v1', // default
  endpoint: 'http://127.0.0.1:8200' // default
  // token: '1234asdf' // optional client token; can be fetched after valid initialization of the server
}

const SECRET_THRESHOLD = 3
const SECRET_SHARES = 5

// get new instance of the client
const vault = require('node-vault')(options)

// init vault server
vault.init({secret_shares: SECRET_SHARES, secret_threshold: SECRET_THRESHOLD})
  .then((result) => {
    console.log('Init: ', JSON.stringify(result, null, 2))
    writeTokenData(result)
    const keys = result.keys

    // set token for all following requests
    vault.token = result.root_token

    // unseal vault server
    return Promise.all(
      keys.slice(-SECRET_THRESHOLD).map(key => {
        console.log('vault operator unseal ', key)
        return vault.unseal({secret_shares: 1, key: key})
      })
    )
  })
  .catch(console.error)

function writeTokenData(data) {
  fs.writeFile('init_token.json', JSON.stringify(data, null, 2), (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
  });
}
