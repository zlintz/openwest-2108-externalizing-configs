process.env.DEBUG = 'node-vault'
const _ = require('lodash')
const token = require('./init_token').root_token

const vault = require('node-vault')({
  token: token
})

const Logger = require('bunyan')
const log = new Logger({name: 'vault-demo'})

function getSecret(name) {
  return vault.read(`secret/${name}`)
    .then((result) => {
      return {key: name, value: result.data.value}
    })
    .catch((err) => {
      log.warn({err: err}, `Error on secret with name: ${name}`)
      throw err
    })
}

const seedSecrets = {
  'postgres/user': 'finn',
  'postgres/password': 'punchYoBuns', // This is better suited for the secrets/database engine
  'my-secret': 'super-s3cret',
  'secret_number': 42,
  'list_o_secrets': [1, '2']
}

async function writeSeedData(seedData) {
  const secretPaths = Object.keys(seedData)
  await Promise.all(secretPaths.map(path =>
    vault.write('secret/' + path, {value: seedData[path]}))
  )
  return secretPaths
}

async function start() {
  const secretPaths = await writeSeedData(seedSecrets)

  const secrets = await Promise.all(secretPaths.map(path => getSecret(path)))

  const combinedSecrets = combine(secrets)

  console.log('Combined Secrets: ', JSON.stringify(combinedSecrets, null, 2))

  setSecretsToNonEnumerable(combinedSecrets)
  console.log('Non enumerable secrets: ', JSON.stringify(combinedSecrets, null, 2))

  // await cleanup()
}

function combine(secretResults) {
  return secretResults.reduce((agg, kv) => {
    return _.set(agg, kv.key.split('/').join('.'), kv.value)
  }, {})
}

function setSecretsToNonEnumerable(secretsObj = {}) {
  function traverseObject(obj) {
    for (let property in obj) {
      if (obj.hasOwnProperty(property)) {
        if (typeof obj[property] === 'object') {
          traverseObject(obj[property])
        }
        Object.defineProperty(obj, property, {enumerable: false}) // non enumerable so they don't get included during a JSON.stringify
      }
    }
  }

  traverseObject(secretsObj)
}

function cleanup() {
  return Promise.all(
    Object.keys(seedSecrets).map(path => vault.delete(`secret/${path}`))
  )
}

start()
