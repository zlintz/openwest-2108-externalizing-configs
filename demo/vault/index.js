const _ = require('lodash')
const vault = require('node-vault')

function Vault (options = {}) {
  if (!(this instanceof Vault)) {
    return new Vault(options)
  }
  if (!options.token) {
    throw new Error('token is required')
  }
  this._vault = vault(options)
}

Vault.prototype.init = function (options = {}) {
  const secretShares = _.get(options, 'secretShares', 5)
  const secretThreshold = _.get(options, 'secretThreshold', 3)

  return vault.init({secret_shares: secretShares, secret_threshold: secretThreshold})
    .then((result) => {
      const keys = result.keys

      // set token for all following requests
      this._vault.token = result.root_token

      // unseal vault server
      return Promise.all(
        keys.slice(-secretThreshold).map(key => {
          console.log('vault operator unseal ', key)
          return vault.unseal({secret_shares: 1, key: key})
        })
      )
    })
    .catch(console.error)
}

Vault.prototype.write = async function (path, data) {
  return this._vault.write('secret/' + path, {value: data})
}

Vault.prototype.get = async function (path) {
  return this._vault.read(`secret/${path}`)
    .then((result) => {
      return {key: path, value: result.data.value}
    })
    .catch((err) => {
      console.log({err: err}, `Error on secret with name: ${path}`)
      throw err
    })
}

Vault.prototype.storeSeedData = async function (seedData) {
  const secretPaths = Object.keys(seedData)
  await Promise.all(secretPaths.map(path =>
    this.write(path, seedData[path])
  ))
}

Vault.prototype.combine = function (secretResults) {
  return secretResults.reduce((agg, kv) => {
    return _.set(agg, kv.key.split('/').join('.'), kv.value)
  }, {})
}

function traversePropsAndSetToNonEnumerable(obj) {
  for (let property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (typeof obj[property] === 'object') {
        traversePropsAndSetToNonEnumerable(obj[property])
      }
      Object.defineProperty(obj, property, {enumerable: false}) // non enumerable so they don't get included during a JSON.stringify
    }
  }
}

Vault.prototype.setSecretsToNonEnumerable = function (secretsObj) {
  traversePropsAndSetToNonEnumerable(secretsObj)
}

Vault.prototype.getSecretsAndCombine = async function (secretPaths) {
  const secrets = await Promise.all(secretPaths.map(path => this.get(path)))
  return this.combine(secrets)
}

Vault.prototype.delete = async function (path) {
  return this._vault.delete(`secret/${path}`)
}

module.exports = Vault
